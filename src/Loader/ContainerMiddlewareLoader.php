<?php

/*
 * This file is part of the Hermes\Pipeline library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Pipeline\Loader;

use Hermes\Pipeline\Container\MiddlewareContainer;
use Hermes\Pipeline\Decorator\CallableMiddleware;
use Hermes\Pipeline\Decorator\LazyMiddleware;
use Hermes\Pipeline\Decorator\RequestHandlerMiddleware;
use Hermes\Pipeline\Exception\InvalidMiddlewareException;
use Hermes\Pipeline\Pipeline;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class ContainerMiddlewareLoader.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class ContainerMiddlewareLoader implements MiddlewareLoader
{
    /**
     * @var MiddlewareContainer
     */
    private $container;

    /**
     * MiddlewareResolver constructor.
     *
     * @param MiddlewareContainer $container
     */
    public function __construct(MiddlewareContainer $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function load($middleware): MiddlewareInterface
    {
        if ($middleware instanceof MiddlewareInterface) {
            return $middleware;
        }
        if ($middleware instanceof RequestHandlerInterface) {
            return $this->handler($middleware);
        }
        if (is_callable($middleware)) {
            return $this->callable($middleware);
        }
        if (is_array($middleware)) {
            return $this->pipeline($middleware);
        }
        if (!is_string($middleware) || '' === $middleware) {
            throw InvalidMiddlewareException::forMiddleware($middleware);
        }

        return $this->lazy($middleware);
    }

    /**
     * @param array $middleware
     *
     * @return MiddlewareInterface
     */
    public function pipeline(array $middleware): MiddlewareInterface
    {
        $pipeline = new Pipeline();
        foreach ($middleware as $individualMiddleware) {
            $pipeline->pipe($this->load($individualMiddleware));
        }

        return $pipeline;
    }

    /**
     * Decorate callable standards-signature middleware via a CallableMiddleware.
     *
     * @param callable $middleware
     *
     * @return CallableMiddleware
     */
    public function callable(callable $middleware): CallableMiddleware
    {
        return new CallableMiddleware($middleware);
    }

    /**
     * Decorate a RequestHandlerInterface as middleware via RequestHandlerMiddleware.
     *
     * @param RequestHandlerInterface $handler
     *
     * @return RequestHandlerMiddleware
     */
    public function handler(RequestHandlerInterface $handler): RequestHandlerMiddleware
    {
        return new RequestHandlerMiddleware($handler);
    }

    /**
     * Create lazy loading middleware based on a service name.
     *
     * @param string $middleware
     *
     * @return LazyMiddleware
     */
    public function lazy(string $middleware): LazyMiddleware
    {
        return new LazyMiddleware($this->container, $middleware);
    }
}
