<?php

/*
 * This file is part of the Hermes\Pipeline library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Pipeline\Loader;

use Hermes\Pipeline\Exception\InvalidMiddlewareException;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class MiddlewareLoader.
 *
 * @author Matías Navarro Carter <mnavarro@option.cl>
 */
interface MiddlewareLoader
{
    /**
     * @param string|array|callable|MiddlewareInterface|RequestHandlerInterface $middleware
     *
     * @return MiddlewareInterface
     *
     * @throws InvalidMiddlewareException if argument is not one of
     *                                    the specified types
     */
    public function load($middleware): MiddlewareInterface;
}
