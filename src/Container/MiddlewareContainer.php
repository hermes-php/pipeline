<?php

/*
 * This file is part of the Hermes\Pipeline library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Pipeline\Container;

use Hermes\Pipeline\Decorator\CallableMiddleware;
use Hermes\Pipeline\Decorator\RequestHandlerMiddleware;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * MiddlewareContainer is a wrapper around Psr11Container to ensure you can only
 * fetch PSR-15 Middleware instances out of it.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class MiddlewareContainer implements ContainerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * MiddlewareContainer constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function has($id): bool
    {
        return $this->container->has($id);
    }

    /**
     * @param string $id
     *
     * @return MiddlewareInterface
     */
    public function get($id): MiddlewareInterface
    {
        $this->logger && $this->logger->debug(sprintf('Loading %s', $id));

        $middleware = $this->container->get($id);

        if (is_callable($middleware)) {
            return new CallableMiddleware($middleware);
        }
        if ($middleware instanceof RequestHandlerInterface) {
            return new RequestHandlerMiddleware($middleware);
        }

        return $middleware;
    }
}
