<?php

/*
 * This file is part of the Hermes\Pipeline library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Pipeline\Exception;

use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class InvalidMiddlewareException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class InvalidMiddlewareException extends \RuntimeException
{
    /**
     * @param mixed $middleware The middleware that does not fulfill the
     *                          expectations of MiddlewarePipe::resolve
     *
     * @return InvalidMiddlewareException
     */
    public static function forMiddleware($middleware): self
    {
        return new self(sprintf(
            'Middleware "%s" is neither a string service name, a PHP callable,'
            .' a %s instance, a %s instance, or an array of such arguments',
            is_object($middleware) ? get_class($middleware) : gettype($middleware),
            MiddlewareInterface::class,
            RequestHandlerInterface::class
        ));
    }
}
