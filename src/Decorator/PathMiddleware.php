<?php

/*
 * This file is part of the Hermes\Pipeline library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Pipeline\Decorator;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class PathMiddleware.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class PathMiddleware implements MiddlewareInterface
{
    /**
     * @var string
     */
    private $path;
    /**
     * @var MiddlewareInterface
     */
    private $middleware;

    /**
     * PathMiddleware constructor.
     *
     * @param string              $path
     * @param MiddlewareInterface $middleware
     */
    public function __construct(string $path, MiddlewareInterface $middleware)
    {
        $this->path = $path;
        $this->middleware = $middleware;
    }

    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->uriMatches($request)) {
            return $this->middleware->process($request, $handler);
        }

        return $handler->handle($request);
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return bool
     */
    private function uriMatches(ServerRequestInterface $request): bool
    {
        return 1 === preg_match(
                '/'.str_replace('/', '\/', $this->path).'/',
                $request->getUri()->getPath()
            );
    }
}
