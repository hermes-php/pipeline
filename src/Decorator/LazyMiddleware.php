<?php

/*
 * This file is part of the Hermes\Pipeline library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Pipeline\Decorator;

use Hermes\Pipeline\Container\MiddlewareContainer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Lazy loads a middleware using a Middleware Container instance.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class LazyMiddleware implements MiddlewareInterface
{
    /**
     * @var MiddlewareContainer
     */
    private $container;
    /**
     * @var string
     */
    private $middlewareId;

    /**
     * LazyMiddleware constructor.
     *
     * @param MiddlewareContainer $container
     * @param string              $middlewareId
     */
    public function __construct(MiddlewareContainer $container, string $middlewareId)
    {
        $this->container = $container;
        $this->middlewareId = $middlewareId;
    }

    /**
     * @param ServerRequestInterface  $request
     * @param RequestHandlerInterface $handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $middleware = $this->container->get($this->middlewareId);

        return $middleware->process($request, $handler);
    }
}
