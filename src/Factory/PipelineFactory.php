<?php

/*
 * This file is part of the Hermes\Pipeline library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Pipeline\Factory;

use Hermes\Pipeline\Pipeline;
use Psr\Container\ContainerInterface;

/**
 * Class PipelineFactory.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class PipelineFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return Pipeline
     */
    public function __invoke(ContainerInterface $container)
    {
        return new Pipeline();
    }
}
