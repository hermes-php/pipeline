<?php

/*
 * This file is part of the Hermes\Pipeline library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Hermes\Pipeline\Factory;

use Hermes\Pipeline\Container\MiddlewareContainer;
use Hermes\Pipeline\Loader\ContainerMiddlewareLoader;
use Psr\Container\ContainerInterface;

/**
 * Class MiddlewareLoaderFactory.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class MiddlewareLoaderFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return ContainerMiddlewareLoader
     */
    public function __invoke(ContainerInterface $container)
    {
        return new ContainerMiddlewareLoader(new MiddlewareContainer($container));
    }
}
